# Welcome to the Small Business Group Wiki
> This wiki is part of this git project but not in the actual repo. To access the wiki pages, click on the 'Wiki' link on the left hand side of the page.

![Sidebar](./side-bar.png)

This project's aim is to gather all of the bits of knowledge, experience, and wisdom that is gleamed from a small group of like minded small business owners. Though many of them come from technical and open source backgrounds, the goal is to track any information that can be useful to a the small business owner; from HR to marketing to support, all topics welcome. Please share with us so we all can do better in running our own businesses.

## New To Gitlab?
The chosen platform for this knowledge, as you can see, is Gitlab. Though this platform is designed to host code, the provided Wiki tool is simple to use and update. However, if you're not used to code repositories, this site can be overwhelming. Let's walk you through how to use the wiki -- you don't need to worry about all the other bells and whistles this platform provides, just how to access the wiki.

### Navigating the Wiki
As noted above, to access the wiki you'll need to click on the 'Wiki' option in the left hand navigation pane. Once you've launched the wiki you'll be dropped onto the "home" page or the main wiki page. From here the various topics will be organized and linked off to other pages. As the knowledge base grows, the way the data is organized may change, so be aware. But for the most part you just click your way through to finding the information you're looking for.

There are a couple of other ways to navigate the wiki pages besides just manually crawling the wiki. Probably the fastest way is to use the search box (in the upper right hand corner of the page) to find all docs that relate to a term or topic.

And the last way to navigate the wiki is through the page view on the right hand side of the page. This just lists a hierarchal view of every wiki page that's attached to the main "home" page.

### Adding to the Wiki
Want to add something to the wiki, a sweet bit about having backups for Yubikeys or maybe how to get your clients to pay on time? Whatever the topic make sure it's related to planning, starting, running, and/or growing small businesses. We'll even accept pages about running a medium sized business \*\*gasp\*\*.

Creating a wiki page is super simple. Below is a step by step instructions on how to do it, if you're not super familiar with repo based wikis. But first, a quick explanation of how these wiki pages are structured.

Think of this wiki as a collection of folders in a filing cabinet. The main page or 'home' is the filing cabinet with all other pages sitting in virtual folders, "folder-slugs", under the main page. Pages can also have sub pages too. In practical terms, each page has the folder concatenated to the front of the title. Below is an example.

```
home -- Kind of like a table of contents
  └ software-suggestions/Time Clock and POS software
  └ Best-Practices-for-Backups/Databases
  └ Best-Practices-for-Backups/Desktop Computers
  └ Best-Practices-for-Backups/Security Tokens
```

1. Navigate to the main wiki page ("home") by clicking on the 'Wiki' navigation link on the left hand side of the page.
2. Once you're on the home page click on the green 'New page' button at the top of the page.
3. You'll be asked for a page slug. This isn't the title, just the name of the folder/file. The slug should be lower case and shouldn't include spaces, use '-' instead of a spaces.
 - The title slug should include the folder-slug (think of it as a category or folder) preceding the title. The format looks like `folder-name/page-title`. For example: 'software-suggestions/time-clock-apps'
4. With the slug chosen you'll land on a 'Create page' page. Here you can give the page the full title (capitalization and spaces are allowed) and the content you want to share.
5. Wiki pages use Markdown as the formatting syntax. If you're not familiar with Markdown just write your content and use the formatting buttons at the top of the body text box.
6. Once you've added all of the details you wish, click on the green 'Create page' button at the bottom of the page.
7. Just above the create button is a text field labeled 'Commit Message'.
 - If you're just creating a page, you can leave this as the default -- the content and title should be explanation enough.
 - If you're editing a page, especially if it's someone else's, please provide a brief explanation of the changes.
8. You've successfully created a page and shared your knowledge!

One more note, If you want to add more content to an additional page, or add a link to your new page on the main page then follow steps 1 and 2 but instead of click on the 'Create page' button click on the 'Edit' button instead. From that point just follow the steps from 6 down.

## New to Markdown?
[Markdown](https://en.wikipedia.org/wiki/Markdown) is a plain text formatting syntax created by John Gruber in 2004. The great advantage to Markdown vs rich text formatting or pure HTML is it's simple to type (unlike HTML) and doesn't require a lot of mouse clicks to select and choose the desired formatting (unlike RTF). The beauty with Markdown is you can use it in any text based application. From Notepad to Libre Office, if you can type text, you can write in Markdown. Having said that, using tools that know what Markdown is much nicer since it'll often highlight the formatting as you're writing and provide a display view to see the fully formatted text.

Gitlab Wiki pages default to being formatted in Markdown. If you're aren't familiar with Markdown you can use the built in formatting buttons at the top of the text box when editing a page. But if you want to learn more about how to use Markdown, here are a few pages that may be worth reading up.

> Quick note: Markdown as John defined it is pretty straight forward. Many groups have wanted extra formatting rules and so have created their own Markdown flavors. Gitlab has done this as well and has added a few extra bits, like strike through, which weren't originally defined by John.

* [John Gruber's Guide to Markdown](https://daringfireball.net/projects/markdown/)
* [Markdown Cheatsheet](https://www.markdownguide.org/cheat-sheet)
* [Gitlab Flavored Markdown Documentation](https://gitlab.com/help/user/markdown#wiki-specific-markdown)